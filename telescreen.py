import sys
import fdb
import os
import argparse
# from contextlib import redirect_stdout


choices = {
    'statements': ['prepare', 'plan', 'start', 'finish', 'free'],
    'procedures': ['start', 'finish'],
    'triggers': ['start', 'finish']
}


def list(connection):
    for session_id, session_params in connection.trace_list().items():
        session_params['id'] = session_id
        if not ('name' in session_params):
            session_params['name'] = ''
        yield session_params


def get_session_id(connection, id_or_name):
    """
    Returns trace session id from id or name
    """
    try:
        id = int(id_or_name)
    except:
        id = None
        if type(id_or_name) is str:
            for session in list(connection):
                if session['name'] == id_or_name:
                    id = session['id']
                    break

        if not id:
            raise Exception("Trace session '{}' doesn't exist".
                            format(id_or_name))
    return id


def print_list(connection, *args, **kwargs):
    print("{id:>6} {name:<25} {date:<19} {user:<16} {flags}".
          format(id='ID', name='Name', date='Date', user='User',
                 flags='Flags'))
    print('=' * 110)
    for session in list(connection):
        print("{id:>6} {name:<25} {date:%Y-%m-%d %H:%M:%S} {user:<16} {flags}".
              format(**session))


def trace_suspend(connection, *args, **kwargs):
    try:
        id = get_session_id(connection, kwargs['id'])
        print(connection.trace_suspend(id))
    except (fdb.fbcore.DatabaseError, Exception) as e:
        print(e)


def trace_resume(connection, *args, **kwargs):
    try:
        id = get_session_id(connection, kwargs['id'])
        print(connection.trace_resume(id))
    except (fdb.fbcore.DatabaseError, Exception) as e:
        print(e)


def trace_stop(connection, *args, **kwargs):
    try:
        id = get_session_id(connection, kwargs['id'])
        print(connection.trace_stop(id))
    except (fdb.fbcore.DatabaseError, Exception) as e:
        print(e)


def trace_start(connection, *args, **kwargs):
    config = """<database {database}>
    enabled                 true

    log_connections         {connections}
    connection_id           {connection_id}
    #include_filter         {include}
    #exclude_filter         {exclude}


    log_transactions        {transactions}

    log_statement_prepare   {statements_prepare}
    log_statement_start     {statements_start}
    log_statement_finish    {statements_finish}
    log_statement_free      {statements_free}

    log_procedure_start     {procedures_start}
    log_procedure_finish    {procedures_finish}

    log_trigger_start       {triggers_start}
    log_trigger_finish      {triggers_finish}

    log_context             {context}

    log_errors              {errors}

    log_sweep               {sweep}


    print_plan              {plan}
    print_perf              {performance}
    max_sql_length          {max_sql_length}
    max_arg_length          {max_arg_length}
    max_arg_count           {max_arg_count}
    time_threshold          {time_threshold}

    max_blr_length          500
    log_blr_requests        false
    print_blr               false

    max_dyn_length          500
    log_dyn_requests        false
    print_dyn               false
</database>
    """

    try:
        dict = kwargs.copy()
        if dict['load']:
            config = dict['load'].read()

        # insert all choices if none was defined in parameters
        # --parameters (without additional choices) became
        # --parameters prepare plan start finish free
        # put those parameters in dictionary for easier config formatting
        for key, value in choices.items():
            if dict[key] == []:
                dict[key] = value
            if dict[key] is None:
                dict[key] = []
            for event in value:
                dict[key + '_' + event] = event in dict[key]
        config = config.format(**dict)

        print(dict)
        print(config)

        if dict['save']:
            dict['save'].write(config)
            dict['save'].close()
            print('Config saved to file')

        id = connection.trace_start(config, dict.get('name', ''))
        print("Trace session {} started".format(id))
        print()
        print("""Use CTRL+BREAK (Windows) or CTRL-SHIFT+\ (*nix) or type
telescreen.py -u <USERNAME> -p <PASSWORD> stop {}
to stop this trace session""".format(id))

        # with redirect_stdout(dict['output']):
        # lahko uporabljam navadni print, brez file= v print
        while True:
            line = connection._QS(fdb.ibase.isc_info_svc_line)
            if not line:
                break
            line = line.rstrip()
            print(line, file=dict['output'])
    except (fdb.fbcore.DatabaseError, Exception) as e:
        print('Trace start Exception')
        print(e)
    except fdb.OperationalError as e:
        print('OperationalError')
        print(e)
    except KeyboardInterrupt as e:
        print("Keyboard Interrupt")


def create_args_parser():
    parser = argparse.ArgumentParser(
        description=''
    )
    parser.add_argument('-o', '--host',
                        default='localhost',
                        help='Server host')
    parser.add_argument('-u', '--username',
                        default=os.environ.get('ISC_USER', 'sysdba'),
                        help="""Username
                        defaults to environment variable ISC_USER
                        or sysdba if not defined""")
    parser.add_argument('-p', '--password',
                        default=os.environ.get('ISC_PASSWORD', None),
                        help="""Password
                        defaults to environment variable ISC_PASSWORD""")
    subparsers = parser.add_subparsers(
        dest='command',
        help="""Type %(prog)s COMMAND -h for additional help for
        command.""")

    # trace_list
    subparsers.add_parser(
        'list',
        help='List trace sessions.')

    # common parser for suspend, stop and resume traces
    parser_common = argparse.ArgumentParser(add_help=False)
    parser_common.add_argument('id',
                               metavar='SESSION_ID',
                               help="Trace or audit Session ID or name")

    # trace_suspend
    subparsers.add_parser(
        'suspend',
        parents=[parser_common],
        help='Suspend trace session.')

    # trace_stop
    subparsers.add_parser(
        'stop',
        parents=[parser_common],
        help='Stop trace session.')

    # trace_resume
    subparsers.add_parser(
        'resume',
        parents=[parser_common],
        help='Resume suspended trace session.')

    # trace_start
    parser_start = subparsers.add_parser(
        'start',
        help='Start trace session.')
    group = parser_start.add_argument_group('Restrictions')
    group.add_argument('--database',
                       metavar='PATTERN',
                       default="",
                       help="""Enable logging only for databases that
                       correspond to pattern. By default %(prog)s logs
                       events from all databases.""")
    group.add_argument('--connection-id', nargs='?',
                       default=0,
                       help="""Trace only given connection id.
                       If zero - trace all connections.""")
    group.add_argument('--include',
                       metavar='PATTERN',
                       default='',
                       help="""Only SQL statements falling under given
                       regular expression are reported in the log.""")
    group.add_argument('--exclude',
                       metavar='PATTERN',
                       default='',
                       help="""SQL statements falling under given
                       regular expression are NOT reported in the
                       log.""")

    group = parser_start.add_argument_group('Events')
    group.add_argument('--connections',
                       action='store_true',
                       help="""Log attach/detach log records""")

    group.add_argument('--transactions',
                       action='store_true',
                       help="""Log transaction start/end records""")

    group.add_argument('--statements',
                       nargs='*',
                       choices=choices['statements'],
                       help="""Log SQL statement execution
                       records""")

    group.add_argument('--procedures',
                       nargs='*',
                       choices=choices['procedures'],
                       help="""Log stored procedure execution""")

    group.add_argument('--triggers',
                       nargs='*',
                       choices=choices['triggers'],
                       help="""Log trigger execute records""")

    group.add_argument('--context',
                       action='store_true',
                       help="""Log context variable change
                       records (RDB$SET_CONTEXT)""")

    group.add_argument('--errors',
                       action='store_true',
                       help="""Log errors happened""")

    group.add_argument('--sweep',
                       action='store_true',
                       help="""Log sweep activity""")

    # Verbose flags
    group = parser_start.add_argument_group('Details')
    group.add_argument('--plan',
                       action='store_true',
                       help="""Print access path (plan) with sql
                       statement""")
    group.add_argument('--performance',
                       action='store_true',
                       help="""Print detailed performance info when
                       applicable""")

    group.add_argument('--max-sql-length',
                       type=int,
                       default=512,
                       help="""Maximum length of SQL string logged
                       Beware when adjusting max_xxx parameters!
                       (default %(default)s)
                       Maximum length of log record for one event should
                       never exceed 64K.""")

    group.add_argument('--max-arg-length',
                       type=int,
                       default=80,
                       help="""Maximum length of individual string
                       argument we log""")
    group.add_argument('--max-arg-count',
                       type=int,
                       default=30,
                       help="""Maximum number of query arguments to put
                       in log""")

    group.add_argument('--time-threshold',
                       type=int,
                       metavar='MS',
                       default=0,
                       help="""Put xxx_finish record only if its
                       timing exceeds this number of milliseconds""")

    parser_start.add_argument('output',
                              nargs='?',
                              type=argparse.FileType('a+'),
                              default=sys.stdout,
                              help="""Save trace output to file""")

    group = parser_start.add_argument_group('Config file')
    group.add_argument('--save',
                       type=argparse.FileType('w'),
                       metavar='CONFIG FILE',
                       help="""Save parameters to config file""")
    group.add_argument('--load',
                       type=argparse.FileType('r'),
                       metavar='CONFIG FILE',
                       help="""Load parameters from config file""")

    return parser


def main():
    parser = create_args_parser()
    args = parser.parse_args()
    dict = vars(args)

    if args.host:
        dict['host'] = dict['host'] + ':service_mgr'
    else:
        dict['host'] = 'service_mgr'

    try:
        con = fdb.services.connect(
            host=dict['host'],
            user=dict['username'],
            password=dict['password'])

        if args.command == 'list':
            print_list(con)
        elif args.command == 'suspend':
            trace_suspend(con, **dict)
        elif args.command == 'resume':
            trace_resume(con, **dict)
        elif args.command == 'stop':
            trace_stop(con, **dict)
        elif args.command == 'start':
            trace_start(con, **dict)
        else:
            parser.print_help()

    except fdb.fbcore.DatabaseError as e:
        print('Exception')
        print(e.args[0])


if __name__ == "__main__":
    main()
