import re
import sys
import argparse
import itertools

start_of_event = r'^\s*(?P<timestamp>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{4}) \((?P<server_process_id>\d+):(?P<internal_id>[A-F0-9]+)\) (?P<event_type>[A-Z_]+)\s*$'
trace_started = r'Trace session ID (?P<session_id>\d+) started'


def raw_event(filename):
    prev_line = ''
    lines = ''
    token = None
    file = open(filename)
    for line in file:
        line = line.rstrip()
        starting_line = re.match(start_of_event, line)
        # start of an event found
        if prev_line == '' and starting_line:
            lines = lines.rstrip()
            if lines and token:
                yield token, lines.split('\n')
            token = starting_line.groupdict()
            lines = ''
        lines = lines + line + '\n'
        prev_line = line.strip()


def chunkstring(string, length):
    """
    Split strnig by fixed length.
    """
    return [string[0 + i:length + i] for i in range(0, len(string), length)]


def parse_raw_event_2(event, lines):
    """
    Parse raw event upwards from line 2. Line must contain first line too!
    Event parameter is dictionary with already decoded first line
    """
    session_line = r'^\s*SESSION_(?P<session_id>\d+)( (?P<session_name>.*))?\s*$'
    database_line = r'^\s*(?P<database>.*) \(ATT_(?P<attachment_id>\d+), (?P<username>\w+):(?P<role>\w+), (?P<characterset>\w+), (?P<protocol>[<>\w]+)(:(?P<address>[\.\w-]+))?\)\s*$'
    process_line = r'^\s*(?P<remote_process>.*):(?P<remote_process_id>\d+)\s*$'
    fetched_line = r'(?P<records_fetched>\d+) records fetched'
    transaction_line = r'^\s*\(TRA_(?P<transaction_id>\d+), (?P<transaction_flags>[\w |]*)\)\s*$'
    transaction_perf = r'^\s*(?P<time>\d+) ms, (?P<writes>\d+) write\(s\), (?P<fetches>\d+) fetch\(es\), (?P<marks>\d+) mark\(s\)\s*$'
    plan_line = r'^(?P<plan>PLAN .*)$'

    def search(pattern, lines, line_no):
        """
        Search pattern in line number line_no
        Function returns dictionary groupdict from regex search, so
        pattern must ge grouped and with names.
        If there is fewer lines than line_no, it returns empty dictionary.
        """
        ret = {}
        if len(lines) > line_no:
            match = re.search(pattern, lines[line_no], re.I)
            if match:
                ret.update(match.groupdict())
        return ret

    def _parse_database_and_process_line_(lines):
        ret = {}
        ret.update(search(database_line, lines, 1))
        ret.update(search(process_line, lines, 2))
        return ret

    def parse_trace_init(lines):
        return search(session_line, lines, 1)

    def parse_trace_finish(lines):
        return search(session_line, lines, 1)

    def parse_attach_database(lines):
        return _parse_database_and_process_line_(lines)

    def parse_unauthorized_attach_database(lines):
        return {}

    def parse_detach_database(lines):
        return _parse_database_and_process_line_(lines)

    def parse_create_database(lines):
        return {}

    def parse_drop_database(lines):
        return {}

    def _parse_transaction_(lines):
        ret = _parse_database_and_process_line_(lines)
        ret.update(search(transaction_line, lines, len(lines) - 1))
        if ret.get('transaction_flags'):
            ret['transaction_flags'] = ret['transaction_flags'].split(' | ')
        return ret

    def _parse_transaction_with_perf_(lines):
        ret = _parse_transaction_(lines[0:-1])
        ret.update(search(transaction_perf, lines[-1:], 0))
        return ret

    def parse_start_transaction(lines):
        ret = _parse_transaction_(lines)
        return ret

    def parse_commit_retaining(lines):
        return {}

    def parse_commit_transaction(lines):
        return _parse_transaction_with_perf_(lines)

    def parse_rollback_retaining(lines):
        return {}

    def parse_rollback_transaction(lines):
        return _parse_transaction_with_perf_(lines)

    def _parse_statement_(lines):
        """
        Parse SQL statement
        """
        ret = {}
        return ret

    def _chunk_table_access_(line):
        keys = ['Natural', 'Index', 'Update', 'Insert', 'Delete',
                'Backout', 'Purge', 'Expunge']
        values = [None if str.strip() == '' else int(str)
                  for str in chunkstring(line, 10)]
        # only return keys with values
        return {k: v for k, v in zip(keys, values) if v}

    def _parse_table_access_(lines):
        ret = {}
        line_no = len(lines)
        while line_no > 0:
            line_no = line_no - 1
            if re.match(r'^\*+$', lines[line_no]):
                break
        if line_no:
            ret = {'table_access':
                   {line[:32].rstrip(): _chunk_table_access_(line[32:])
                    for line in lines[line_no + 1:]}}
        return ret

    def _parse_plan_(lines):
        ret = {}
        line_no = len(lines)
        found = None
        while line_no > 0:
            line_no = line_no - 1
            if lines[line_no] == ('^' * 79):
                found = line_no + 1
                break
        if found:
            ret.update(search(plan_line, lines, found))
        return ret

    def parse_prepare_statement(lines):
        ret = {}
        ret.update(_parse_transaction_(lines[:4]))
        del lines[:4]
        ret.update(_parse_plan_(lines))
        return ret

    def parse_execute_statement_start(lines):
        ret = {}
        ret.update(_parse_transaction_(lines[:4]))
        del lines[:4]
        ret.update(_parse_plan_(lines))
        return ret

    def parse_execute_statement_finish(lines):
        ret = {}
        ret.update(_parse_transaction_(lines[:4]))
        del lines[:4]
        tmp = _parse_table_access_(lines)
        if tmp:
            ret.update(tmp)
            del lines[(0 - len(ret['table_access'])):]
        ret.update(_parse_plan_(lines))

        # read statement
        # read plan
        # read records fetched
        # read time, reads, fetches
        return ret

    def parse_close_cursor(lines):
        ret = _parse_database_and_process_line_(lines)
        del lines[:4]
        ret.update(_parse_plan_(lines))
        # read statement
        return ret

    def parse_free_statement(lines):
        ret = _parse_database_and_process_line_(lines[:3])
        del lines[:4]
        ret.update(_parse_plan_(lines))
        # parse statement
        return ret

    def parse_execute_procedure_start(lines):
        return {}

    def parse_execute_procedure_finish(lines):
        return {}

    def parse_execute_trigger_start(lines):
        return {}

    def parse_execute_trigger_finish(lines):
        return {}

    def parse_set_context(lines):
        return {}

    def parse_sweep_start(lines):
        return {}

    def parse_sweep_progress(lines):
        return {}

    def parse_sweep_finish(lines):
        return {}

    def parse_error(lines):
        return {}

    def parse_unknown(lines):
        return {}

    event_type = {
        'TRACE_INIT': parse_trace_init,
        'TRACE_FINI': parse_trace_finish,
        # 'TRACE_STOP',
        # 'TRACE_SUSP',
        #
        'ATTACH_DATABASE': parse_attach_database,
        'UNAUTHORIZED ATTACH_DATABASE': parse_unauthorized_attach_database,
        'DETACH_DATABASE': parse_detach_database,
        'CREATE_DATABASE': parse_create_database,
        'DROP_DATABASE': parse_drop_database,
        #
        'START_TRANSACTION': parse_start_transaction,
        'COMMIT_RETAINING': parse_commit_retaining,
        'COMMIT_TRANSACTION': parse_commit_transaction,
        'ROLLBACK_RETAINING': parse_rollback_retaining,
        'ROLLBACK_TRANSACTION': parse_rollback_transaction,
        #
        'PREPARE_STATEMENT': parse_prepare_statement,
        'EXECUTE_STATEMENT_START': parse_execute_statement_start,
        'EXECUTE_STATEMENT_FINISH': parse_execute_statement_finish,
        'CLOSE_CURSOR': parse_close_cursor,
        'FREE_STATEMENT': parse_free_statement,
        #
        'EXECUTE_PROCEDURE_START': parse_execute_procedure_start,
        'EXECUTE_PROCEDURE_FINISH': parse_execute_procedure_finish,
        #
        'EXECUTE_TRIGGER_START': parse_execute_trigger_start,
        'EXECUTE_TRIGGER_FINISH': parse_execute_trigger_finish,
        #
        'SET_CONTEXT': parse_set_context,
        #
        'SWEEP_START': parse_sweep_start,
        'SWEEP_PROGRESS': parse_sweep_progress,
        'SWEEP_FINISH': parse_sweep_finish,
        #
        'ERROR AT \w+': parse_error,
        # unknown event
        '.*': parse_unknown}

    parse_func = event_type.get(event['event_type'])
    if parse_func:
        ret = parse_func(lines)
    else:
        ret = {}
        print("parse func not found")
    return ret


def parse_raw_event(lines):
    """
    Parse single event from trace session.
    """
    lines = lines.strip().split('\n')
    if not lines:
        return {}

    starting_line = re.match(start_of_event, lines[0])
    if starting_line:
        token = starting_line.groupdict()
        token.update(parse_raw_event_2(token, lines))
    return token


def parse_file(filename):
    for event, lines in raw_event(filename):
        tmp = parse_raw_event_2(event, lines.copy())
        event.update(tmp)
        yield event, lines, not tmp


"""
def print_event(event):
    for key, value in event.items():
        print("{0:<20}: {1}".format(key, value))
"""


def main():
    parser = argparse.ArgumentParser(
        description=''
    )
    parser.add_argument('filename')
    args = parser.parse_args()

    for event, lines, unresolved in parse_file(args.filename):
        print('=' * 100)
        for line in lines:
            print(line)
        print('- ' * 50)
        if unresolved:
            print("resolve error " + '*' * 120)
            print()
        for key, value in event.items():
            print("{0:<20}: {1}".format(key, value))

    """
    for event, lines in raw_event("log/audit.2017-09-18T13-48-56.log"):
        print(event)
        for line in lines:
            print(line, end='')
        print('*'*80)
    """

    """
    for prev, line in read2lines("audit.2017-09-18T13-48-56.log"):
        print("1: " + prev)
        print("2: " + line)

    try:
        log = open("audit.2017-09-18T13-48-56.log", "r")
        for event in parse_file(log):
            print(event)
    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
    else:
        log.close()
    """


if __name__ == "__main__":
    main()
